﻿using Board;
using DG.Tweening;

namespace Utils
{
    public static class TweeningUtility
    {
        public static Sequence MoveSequence(TileController tileA, TileController tileB, TweenCallback tweenCallback)
        {
            var moveSequence = DOTween.Sequence();
            moveSequence.Join(tileA.Move(tileB.StartDragPosition));
            moveSequence.Join(tileB.Move(tileA.StartDragPosition));
            moveSequence.AppendCallback(tweenCallback);
            return moveSequence;
        }

        public static Sequence ReturnToStartPositionSequence(TileController tileA, TileController tileB, TweenCallback tweenCallback)
        {
            var returnToStartSequence = DOTween.Sequence();
            returnToStartSequence.Join(tileA.ReturnToStartPosition());
            returnToStartSequence.Join(tileB.ReturnToStartPosition());
            returnToStartSequence.PrependCallback(tweenCallback);
            return returnToStartSequence;
        }


        public static Sequence PopSequence(TileController tile, TweenCallback callback, bool doSequence = true)
        {
            var sequence = DOTween.Sequence();
            sequence.Append(tile.ScaleUp());
            sequence.Append(tile.ScaleDown());

            if (doSequence)
                sequence.AppendCallback(callback);
            else
                callback();

            return sequence;
        }
    }
}
