﻿using UnityEngine;

namespace Events
{
    [CreateAssetMenu(fileName = "OnSpecialTileDragEndedObserver", menuName = "EventObservers/OnSpecialTileDragEndedObserver")]
    public class OnSpecialTileDragEndedObserver : EventObserver<OnSpecialTileDragEndedData>
    {
    }
}