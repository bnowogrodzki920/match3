﻿using UnityEngine;

namespace Events
{
    [CreateAssetMenu(fileName = "OnTilePoppedObserver", menuName = "EventObservers/OnTilePoppedObserver")]
    public class OnTilePoppedObserver : EventObserver<OnTilePoppedData>
    {
    }
}