using UnityEngine;

namespace Events
{
    [CreateAssetMenu(fileName = "OnTileDragEndedObserver", menuName = "EventObservers/OnTileDragEndedObserver")]
    public class OnTileDragEndedObserver : EventObserver<OnTileDragEndedData>
    {
    }
}