﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public abstract class EventObserver<TEventData> : ScriptableObject
    {
        private List<IListener<TEventData>> listeners = new List<IListener<TEventData>>();

        public void AddListener(IListener<TEventData> listener)
        {
            if (!listeners.Contains(listener)) listeners.Add(listener);
        }

        public void RemoveListener(IListener<TEventData> listener)
        {
            if (listeners.Contains(listener)) listeners.Remove(listener);
        }

        public void Invoke(TEventData data)
        {
            foreach (var listener in listeners)
            {
                try
                {
                    listener.OnEventInvoked(data);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
    }
}
