﻿namespace Events
{
    public interface IListener<TEventData>
    {
        void OnEventInvoked(TEventData eventData);
    }
}
