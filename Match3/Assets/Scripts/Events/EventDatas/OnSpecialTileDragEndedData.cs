﻿using Board;
using System;

namespace Events
{
    public class OnSpecialTileDragEndedData
    {
        public OnSpecialTileDragEndedData(TileController specialTile, TileController tileB)
        {
            SpecialTile = specialTile ?? throw new ArgumentException(nameof(specialTile));
            TileB = tileB ?? throw new ArgumentException(nameof(tileB));
        }

        public TileController SpecialTile { get; private set; }
        public TileController TileB { get; private set; }
    }
}
