﻿using Board;
using System;

namespace Events
{
    public class OnTilePoppedData
    {
        public OnTilePoppedData(TileController tile)
        {
            Tile = tile ?? throw new ArgumentException(nameof(tile));
        }

        public TileController Tile { get; private set; }
    }
}
