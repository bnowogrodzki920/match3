﻿using Board;
using System;

namespace Events
{
    public class OnTileDragEndedData
    {
        public OnTileDragEndedData(TileController tileA, TileController tileB)
        {
            TileA = tileA ?? throw new ArgumentException(nameof(tileA));
            TileB = tileB ?? throw new ArgumentException(nameof(tileB));
        }

        public TileController TileA { get; private set; }
        public TileController TileB { get; private set; }
    }
}
