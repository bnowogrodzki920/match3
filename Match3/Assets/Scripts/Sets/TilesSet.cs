﻿using UnityEngine;
using Sets;

namespace Board
{
    [CreateAssetMenu(fileName = "TilesSet", menuName = "Sets/TilesSet")]
    public class TilesSet : RuntimeSet<TileController>
    {
    }
}