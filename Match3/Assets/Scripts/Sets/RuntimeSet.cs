﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Sets
{
    public class RuntimeSet<TObject> : ScriptableObject
    {
        private List<TObject> data = new List<TObject>();

        public void Add(TObject obj)
        {
            if (!data.Contains(obj)) data.Add(obj);
        }

        public bool Remove(TObject obj)
        {
            return data.Remove(obj);
        }

        public bool Contains(TObject obj)
        {
            return data.Contains(obj);
        }


        public bool TryGetValue(Predicate<TObject> predicate, out TObject value)
        {
            value = data.FirstOrDefault(data => predicate(data));
            return value != null;
        }

        public bool TryGetAllValues(Predicate<TObject> predicate, out List<TObject> values)
        {
            values = data.FindAll((a) => predicate(a));
            return values != null && values.Count > 0;
        }

        public TObject this[int index]
        {
            get => data[index];
        }

        public int Count => data.Count;
    }
}
