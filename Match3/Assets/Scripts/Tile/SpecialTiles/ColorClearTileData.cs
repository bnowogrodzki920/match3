﻿using System.Collections.Generic;
using UnityEngine;

namespace Board.Tiles
{
    [CreateAssetMenu(fileName = "ColorClear", menuName = "Board/Tiles/ColorClearTile")]
    public class ColorClearTileData : SpecialTileData
    {
        public TileData lastMovedTile { get; set; }

        public override void SpecialPop(TilesSet tilesSet, TileController tile, out List<TileController> matchingTiles)
        {
            tilesSet.TryGetAllValues(a => a.TileData == lastMovedTile, out matchingTiles);
            matchingTiles.Add(tile);
        }
    }
}
