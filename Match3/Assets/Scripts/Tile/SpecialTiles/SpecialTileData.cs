﻿using System.Collections.Generic;

namespace Board.Tiles
{
    public abstract class SpecialTileData : TileData
    {
        public abstract void SpecialPop(TilesSet tilesSet, TileController tile, out List<TileController> matchingTiles);
    }
}
