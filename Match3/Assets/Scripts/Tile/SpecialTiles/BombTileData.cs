﻿using System.Collections.Generic;
using UnityEngine;

namespace Board.Tiles
{
    [CreateAssetMenu(fileName = "Bomb", menuName = "Board/Tiles/BombTile")]
    public class BombTileData : SpecialTileData
    {
        public override void SpecialPop(TilesSet tilesSet, TileController tile, out List<TileController> matchingTiles)
        {
            matchingTiles = new List<TileController>();
            for (int y = -1; y <= 1; y++)
            {
                for (int x = -1; x <= 1; x++)
                {
                    var offsetedPos = tile.GridPosition + new Vector2Int(x, y);
                    if (tilesSet.TryGetValue(a => a.GridPosition == offsetedPos, out var matchingTile) && !matchingTiles.Contains(matchingTile))
                        matchingTiles.Add(matchingTile);
                }
            }
        }
    }
}
