﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Board
{
    public class TileView : MonoBehaviour
    {
        [SerializeField]
        private Image tileImage;
        [SerializeField]
        private Image backgroundImage;

        [Header("Tweening")]
        [SerializeField]
        private float moveTweenDuration = 0.5f;
        [SerializeField]
        private float scaleUpMultiplier = 1.3f;
        [SerializeField]
        private float scaleUpTweenDuration = 0.3f;
        [SerializeField]
        private float scaleDownTweenDuration = 0.3f;

        public void SetData(Sprite sprite)
        {
            tileImage.sprite = sprite;
        }

        public void ToggleRaycastTarget(bool value)
        {
            backgroundImage.raycastTarget = value;
        }

        public void SetSiblingIndex(int index)
        {
            transform.SetSiblingIndex(index);
        }

        public int GetSiblingIndex()
        {
            return transform.GetSiblingIndex();
        }

        public Tween Move(Vector3 newPos)
        {
            return transform.DOMove(newPos, moveTweenDuration);
        }

        public Tween ScaleUp()
        {
            return transform.DOScale(Vector3.one * scaleUpMultiplier, scaleUpTweenDuration);
        }

        public Tween ScaleDown()
        {
            return transform.DOScale(Vector3.one, scaleDownTweenDuration);
        }
    }
}
