﻿using UnityEngine;

namespace Board.Tiles
{
    [CreateAssetMenu(fileName = "TileData", menuName = "Board/Tiles/NormalTileData")]
    public class TileData : ScriptableObject
    {
        [field: SerializeField]
        public int Score { get; private set; }
        [field: SerializeField]
        public Sprite Sprite { get; private set; }

        public override bool Equals(object other)
        {
            TileData otherData = other as TileData;
            return otherData == null
                || (this.Score == otherData.Score
                && this.Sprite == otherData.Sprite);
        }
    }
}
