﻿using Board.Tiles;
using DG.Tweening;
using Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Board
{
    [SelectionBase]
    public class TileController : MonoBehaviour, IBeginDragHandler, IDropHandler, IDragHandler, IEndDragHandler, IListener<OnTilePoppedData>
    {
        [SerializeField]
        private TileView tileView;
        [SerializeField]
        private OnTileDragEndedObserver onTileDragEndedObserver;
        [SerializeField]
        private OnTilePoppedObserver onTilePoppedObserver;
        [SerializeField]
        private OnSpecialTileDragEndedObserver onSpecialTileDragEndedObserver;
        [SerializeField]
        private TilesSet tilesSet;

        private TileData tileData;
        private Vector2Int gridPosition;
        private Vector2 startDragPosition;
        private int prevSiblingIndex; 

        public TileData TileData
        {
            get => tileData;
            set
            {
                this.tileData = value;
                tileView.SetData(value.Sprite);
                ResetDragPosition();
            }
        }

        public Vector2Int GridPosition
        {
            get => gridPosition;
            set => gridPosition = value;
        }

        public Vector2 StartDragPosition
        {
            get => startDragPosition;
            set => startDragPosition = value;
        }

        public int PrevSiblingIndex => prevSiblingIndex;

        #region UnityMessages
        private void Awake()
        {
            tilesSet.Add(this);
            onTilePoppedObserver.AddListener(this);
            ResetDragPosition();
        }

        private void OnDestroy()
        {
            tilesSet.Remove(this);
            onTilePoppedObserver.RemoveListener(this);
        }
        #endregion

        public void OnBeginDrag(PointerEventData pointerData)
        {
            ResetDragPosition();
            prevSiblingIndex = transform.GetSiblingIndex();
            transform.SetAsFirstSibling();
        }

        public void OnDrag(PointerEventData pointerData)
        {
            var newPos = pointerData.position;

            transform.position = newPos;
        }

        public void OnEndDrag(PointerEventData pointerData)
        {
            if (pointerData.pointerDrag == pointerData.pointerEnter)
                ReturnToStartPosition().Play();

        }

        public void OnDrop(PointerEventData pointerData)
        {
            if (pointerData.pointerDrag != pointerData.pointerEnter)
            {
                var draggedTile = pointerData.pointerDrag.GetComponent<TileController>();

                if (IsNeighbourTile(draggedTile))
                {
                    if(tileData is SpecialTileData)
                    {
                        if(tileData is ColorClearTileData colorClearTileData)
                            colorClearTileData.lastMovedTile = draggedTile.TileData;

                        Debug.Log("SpecialTile popped");
                        onSpecialTileDragEndedObserver.Invoke(new OnSpecialTileDragEndedData(this, draggedTile));
                    }
                    else if(draggedTile.tileData is SpecialTileData)
                    {
                        if(draggedTile.TileData is ColorClearTileData colorClearTileData)
                            colorClearTileData.lastMovedTile = tileData;

                        Debug.Log("SpecialTile popped");
                        onSpecialTileDragEndedObserver.Invoke(new OnSpecialTileDragEndedData(draggedTile, this));
                    }
                    else
                    {
                        onTileDragEndedObserver.Invoke(new OnTileDragEndedData(draggedTile, this));
                    }
                }
                else
                {
                    draggedTile.ReturnToStartPosition().Play();
                }
            }
            else
            {
                ReturnToStartPosition().Play();
            }
        }

        public void SetSiblingIndex(int index)
        {
            tileView.SetSiblingIndex(index);
        }
        
        public int GetSiblingIndex()
        {
            return tileView.GetSiblingIndex();
        }

        public bool IsNeighbourTile(TileController tile)
        {
            return (this.gridPosition.x + 1 == tile.gridPosition.x && this.gridPosition.y == tile.gridPosition.y)
                || (this.gridPosition.x - 1 == tile.gridPosition.x && this.gridPosition.y == tile.gridPosition.y)
                || (this.gridPosition.y + 1 == tile.gridPosition.y && this.gridPosition.x == tile.gridPosition.x)
                || (this.gridPosition.y - 1 == tile.gridPosition.y && this.gridPosition.x == tile.gridPosition.x);
        }


        #region Tweening
        public Tween ReturnToStartPosition()
        {
            Debug.Log($"{this} move towards {startDragPosition}");
            return tileView.Move(startDragPosition);
        }

        public Tween Move(Vector2 newPos)
        {
            Debug.Log($"{this} move towards {newPos}");
            return tileView.Move(newPos);
        }

        public Tween ScaleUp()
        {
            return tileView.ScaleUp();
        }

        public Tween ScaleDown()
        {
            return tileView.ScaleDown();
        }
        #endregion

        public void ResetDragPosition()
        {
            startDragPosition = transform.position;
        }

        public void OnEventInvoked(OnTilePoppedData data)
        {
            if (data.Tile != this) return;
            ResetDragPosition();
        }

        public void ToggleInteractions(bool v)
        {
            tileView.ToggleRaycastTarget(v);
        }
    }
}