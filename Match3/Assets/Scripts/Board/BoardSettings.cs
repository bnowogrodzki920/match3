﻿using UnityEngine;

namespace Board
{
    [CreateAssetMenu(fileName = "BoardSettings", menuName = "Board/Settings")]
    public class BoardSettings : ScriptableObject
    {
        [field: SerializeField, Tooltip("Distance from screen border")]
        public Vector2Int BorderOffset { get; private set; } = new Vector2Int(100, 50);
        [field: SerializeField]
        public Vector2Int BoardSize { get; private set; } = new Vector2Int(10, 10);
        [field: SerializeField]
        public Vector2 TileSpacing { get; private set; } = new Vector2(0.5f, 0.5f);
        [field: SerializeField]
        public string TilesPath { get; private set; } = "Tiles";
        [field: SerializeField]
        public string SpecialTilesPath { get; private set; } = "SpecialTiles";
        [field: SerializeField, Range(0,1)]
        public float SpecialTileChance { get; private set; } = 0.05f;
        [field: SerializeField]
        public bool UseRandomSeed { get; private set; } = true;
        [field: SerializeField]
        public int Seed { get; private set; } = 0;
        [field: SerializeField]
        public TileController TilePrefab { get; private set; }
    }
}
