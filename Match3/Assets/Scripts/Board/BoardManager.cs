using Board.Tiles;
using DG.Tweening;
using Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace Board
{
    public class BoardManager : MonoBehaviour, IListener<OnTileDragEndedData>, IListener<OnSpecialTileDragEndedData>
    {
        [SerializeField]
        private BoardSettings settings;
        [SerializeField]
        private OnTileDragEndedObserver onTileDragEndedObserver;
        [SerializeField]
        private OnTilePoppedObserver onTilePoppedObserver;
        [SerializeField]
        private OnSpecialTileDragEndedObserver onSpecialTileDragEndedObserver;
        [SerializeField]
        private TilesSet tilesSet;

        private TileData[] tiles;
        private SpecialTileData[] specialTiles;

        #region UnityMessages
        private void Awake()
        {
            onTileDragEndedObserver.AddListener(this);
            onSpecialTileDragEndedObserver.AddListener(this);
            tiles = GetTiles(settings.TilesPath);
            specialTiles = GetSpecialTiles(settings.SpecialTilesPath);
            Random.InitState(settings.UseRandomSeed ? DateTime.Now.GetHashCode() : settings.Seed);
        }

        private TileData[] GetTiles(string tilesPath)
        {
            return Resources.LoadAll<TileData>(tilesPath);
        }

        private SpecialTileData[] GetSpecialTiles(string specialTilesPath)
        {
            return Resources.LoadAll<SpecialTileData>(specialTilesPath);
        }

        private IEnumerator Start()
        {
            GenerateGrid();
            var routine = StartCoroutine(TryToDoCombo(false));
            yield return routine;

            // manually add two first special items
            tilesSet[Random.Range(0, tilesSet.Count)].TileData = specialTiles[0];
            tilesSet[Random.Range(0, tilesSet.Count)].TileData = specialTiles[1];
        }

        private void OnDestroy()
        {
            onTileDragEndedObserver.RemoveListener(this);
            onSpecialTileDragEndedObserver.RemoveListener(this);
        }
        #endregion

        private void GenerateGrid()
        {
            var xSize = settings.BoardSize.x;
            var ySize = settings.BoardSize.y;

            var halfSizeDelta = (settings.TilePrefab.transform as RectTransform).sizeDelta / 2f;
            var screenMiddle = new Vector2(Screen.width / 2f, Screen.height / 2f);
            var startPos = new Vector2(screenMiddle.x - (halfSizeDelta.x + settings.TileSpacing.x) * (xSize / 2),
                                       screenMiddle.y + (halfSizeDelta.y + settings.TileSpacing.y) * (ySize / 2));

            if (xSize % 2 == 0)
                startPos.x += halfSizeDelta.x - settings.TileSpacing.x / 2;
            if (ySize % 2 == 0)
                startPos.y -= halfSizeDelta.y - settings.TileSpacing.y / 2;

            Debug.Log(startPos);
            for (int y = 0; y < ySize; y++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    var pos = new Vector2(startPos.x + (halfSizeDelta.x + settings.TileSpacing.x) * x,
                                          startPos.y - (halfSizeDelta.y + settings.TileSpacing.y) * y);

                    var tile = Instantiate(settings.TilePrefab, pos, Quaternion.identity, transform);
                    tile.name = $"Tile {x} {y}";
                    tile.GridPosition = new Vector2Int(x, y);
                    tile.TileData = GetRandomTileData();
                }
            }

        }

        private TileData GetRandomTileData()
        {
            if(Random.value < settings.SpecialTileChance)
                return specialTiles[Random.Range(0, specialTiles.Length)];

            return tiles[Random.Range(0, tiles.Length)];
        }

        public void OnEventInvoked(OnSpecialTileDragEndedData eventData)
        {
            StartCoroutine(CheckMatchesFor(eventData.SpecialTile, eventData.TileB));
        }

        public void OnEventInvoked(OnTileDragEndedData eventData)
        {
            StartCoroutine(CheckMatchesFor(eventData.TileA, eventData.TileB));
        }

        private IEnumerator CheckMatchesFor(TileController tileA, TileController tileB)
        {
            ToggleInteractionsOnTiles(false);
            Sequence moveSequence = TweeningUtility.MoveSequence(tileA, tileB, () =>
            {
                SwapTiles(tileA, tileB);
            });
            yield return moveSequence.Play().WaitForCompletion();

            bool hasMatchesForTileA = HasMatchFor(tileA, out var matchingTilesA);
            bool hasMatchesForTileB = HasMatchFor(tileB, out var matchingTilesB);

            if (!hasMatchesForTileA && !hasMatchesForTileB)
            {
                Sequence returnToStartSequence = TweeningUtility.ReturnToStartPositionSequence(tileA, tileB, () => SwapTiles(tileA, tileB));
                yield return returnToStartSequence.Play().WaitForCompletion();
            }
            else
            {
                var temp = tileA.StartDragPosition;
                tileA.StartDragPosition = tileB.StartDragPosition;
                tileB.StartDragPosition = temp;

                var tilesToPop = new List<TileController>();
                if(hasMatchesForTileA)
                    tilesToPop.AddRange(matchingTilesA);
                if(hasMatchesForTileB)
                    tilesToPop.AddRange(matchingTilesB);

                Sequence popSequence = DOTween.Sequence();
                foreach(var tile in tilesToPop)
                {
                    popSequence.Join(TweeningUtility.PopSequence(tile, () =>
                    {
                        tile.TileData = GetRandomTileData();
                        onTilePoppedObserver.Invoke(new OnTilePoppedData(tile));
                    }));
                }
                yield return popSequence.Play().WaitForCompletion();

                IEnumerator comboEnumerator = TryToDoCombo(true);
                while (comboEnumerator.MoveNext())
                    yield return comboEnumerator.Current;
            }
            for (int i = 0; i < 5; i++)
            {
                yield return null; // wait 5 frames to ensure that every tween has ended
            }
            while (!ArePossibleMovesOnBoard())
            {
                Shuffle();
                Debug.Log("Shuffle");
                IEnumerator comboEnumerator = TryToDoCombo(true);
                while (comboEnumerator.MoveNext())
                    yield return comboEnumerator.Current;
            }
            Debug.Log("No need to shuffle");

            ToggleInteractionsOnTiles(true);
        }

        private bool HasMatchFor(TileController tile, out List<TileController> matchingTiles)
        {
            matchingTiles = new List<TileController>();
            var queue = new Queue<TileController>();
            queue.Enqueue(tile);

            while (queue.Count > 0)
            {
                var currentTile = queue.Dequeue();

                if (!matchingTiles.Contains(currentTile))
                    matchingTiles.Add(currentTile);

                if (tilesSet.TryGetAllValues((a) =>
                    {
                        return IsTheSameTypeAs(tile, a) && currentTile.IsNeighbourTile(a);
                    }, out var neighbourTiles))
                {
                    foreach (var neighbourTile in neighbourTiles)
                    {
                        if (!matchingTiles.Contains(neighbourTile) && !matchingTiles.Any((a) => a.GridPosition == neighbourTile.GridPosition))
                        {
                            queue.Enqueue(neighbourTile);
                        }
                    }
                }
            }

            if(tile.TileData is SpecialTileData specialTileData)
            {
                specialTileData.SpecialPop(tilesSet, tile, out matchingTiles);
                return true;
            }

            bool hasHorizontalMatch = CheckAxis(tile.GridPosition.x, true, matchingTiles);
            bool hasVerticalMatch = CheckAxis(tile.GridPosition.y, false, matchingTiles);
            RemoveUnmatchedTiles(tile, matchingTiles, hasHorizontalMatch, hasVerticalMatch);

            return hasHorizontalMatch || hasVerticalMatch;
        }

        private void RemoveUnmatchedTiles(TileController tile, List<TileController> matchingTiles, bool hasHorizontalMatch, bool hasVerticalMatch)
        {
            if (hasHorizontalMatch && !hasVerticalMatch)
            {
                matchingTiles.RemoveAll((a) => a.GridPosition.x != tile.GridPosition.x);
            }
            if (hasVerticalMatch && !hasHorizontalMatch)
            {
                matchingTiles.RemoveAll((a) => a.GridPosition.y != tile.GridPosition.y);
            }
        }

        private bool IsTheSameTypeAs(TileController firstTile, TileController tileToCheck)
        {
            return firstTile.TileData.Equals(tileToCheck.TileData);
        }

        private bool CheckAxis(int startPos, bool isHorizontal, List<TileController> matchingTiles)
        {
            return matchingTiles.FindAll((tile) => isHorizontal ? tile.GridPosition.x == startPos : tile.GridPosition.y == startPos).Count >= 3;
        }

        private IEnumerator TryToDoCombo(bool doSequence)
        {
            var xSize = settings.BoardSize.x;
            var ySize = settings.BoardSize.y;

            var tiles = new List<TileController>();

            while (true)
            {
                for (int y = 0; y < ySize; y++)
                {
                    for (int x = 0; x < xSize; x++)
                    {
                        if (tilesSet.TryGetValue((a) => a.GridPosition == new Vector2Int(x, y), out var tile)
                            && tile.TileData is not SpecialTileData
                            && HasMatchFor(tile, out var matchingTiles))
                        {
                            foreach (var matchingTile in matchingTiles)
                            {
                                if (!tiles.Contains(matchingTile))
                                {
                                    tiles.Add(matchingTile);
                                }
                            }
                        }
                    }
                }
                if (tiles.Count == 0) break;

                var poppingSequence = DOTween.Sequence();
                foreach (var tile in tiles)
                {
                    poppingSequence.Join(TweeningUtility.PopSequence(tile, () =>
                    {
                        tile.TileData = GetRandomTileData();
                        onTilePoppedObserver.Invoke(new OnTilePoppedData(tile));
                    }, doSequence));
                }

                if (doSequence)
                {
                    poppingSequence.Play();
                    yield return poppingSequence.WaitForCompletion();
                }

                tiles.Clear();
            }
        }

        private void Shuffle()
        {
            for (int i = 0; i < tilesSet.Count; i++)
            {
                tilesSet[i].TileData = GetRandomTileData();
            }
        }

        private bool ArePossibleMovesOnBoard()
        {
            var xSize = settings.BoardSize.x;
            var ySize = settings.BoardSize.y;

            List<TileController> checkedTiles = new List<TileController>();
            for (int y = 0; y < ySize; y++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    int index = x + y * ySize;
                    var currentTile = tilesSet[index];
                    if (checkedTiles.Contains(currentTile))
                        continue;

                    if (ArePossibleMovesFor(currentTile))
                        return true;
                    checkedTiles.Add(currentTile);
                }
            }
            return false;
        }

        private bool ArePossibleMovesFor(TileController tile)
        {
            //  _ _ _
            // |c|x|c|
            // |_|c|_|
            //
            // x - initial position
            // c - positions to check
            // can ignore up because it should be already checked by the tile above

            var gridPosition = tile.GridPosition;
            
            tile.GridPosition += Vector2Int.right;
            if(HasMatchFor(tile, out _))
            {
                tile.GridPosition = gridPosition;
                return true;
            }
            tile.GridPosition = gridPosition;

            tile.GridPosition += Vector2Int.left;
            if(HasMatchFor(tile, out _))
            {
                tile.GridPosition = gridPosition;
                return true;
            }
            tile.GridPosition = gridPosition;
            
            tile.GridPosition += Vector2Int.up; // bottom is 1 y higher
            if(HasMatchFor(tile, out _))
            {
                tile.GridPosition = gridPosition;
                return true;
            }
            tile.GridPosition = gridPosition;

            return false;
        }

        private void SwapTiles(TileController tileA, TileController tileB)
        {
            var tempGridPos = tileA.GridPosition;
            tileA.GridPosition = tileB.GridPosition;
            tileB.GridPosition = tempGridPos;


            int prevSiblingIndex = tileA.GetSiblingIndex();
            tileB.SetSiblingIndex(tileA.PrevSiblingIndex);
            tileA.SetSiblingIndex(prevSiblingIndex);
        }

        private void ToggleInteractionsOnTiles(bool v)
        {

            for (int i = 0; i < tilesSet.Count; i++)
            {
                tilesSet[i].ToggleInteractions(v);
            }
        }
    }
}
